
#include <iostream>
#include <vector>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	Vector()  : x(4), y(5), z(4)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double size()
	{
		 return sqrt(x * x + y * y + z * z);
		 
	}

};



int main()
{
	Vector v;
	cout << v.size();
}